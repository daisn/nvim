-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------- GENERAL SETTINGS ---------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
vim.opt.nu = true
vim.opt.rnu = true
vim.opt.wrap = false
vim.opt.ruler = true
vim.opt.smarttab = true
vim.opt.expandtab = true
vim.opt.splitright = true
vim.opt.splitbelow = true
vim.opt.autoindent = true
vim.opt.ignorecase = true
vim.opt.cursorline = true
vim.opt.smartindent = true
vim.opt.termguicolors = true
vim.opt.mouse = 'a'
vim.opt.tabstop = 4
vim.opt.cmdheight = 2
vim.opt.pumheight = 10
vim.opt.laststatus = 0
vim.opt.shiftwidth = 2
vim.opt.updatetime = 250
vim.opt.timeoutlen = 400
vim.g.mapleader = ','
vim.opt.iskeyword:append('-')
vim.opt.clipboard = 'unnamedplus'
vim.opt.formatoptions = vim.opt.formatoptions - 'cro'
