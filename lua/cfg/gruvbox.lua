-- gruvbox-flat
vim.g.gruvbox_sidebars = {"terminal"}
vim.g.gruvbox_italic_functions = true
vim.g.gruvbox_colors = { hint = "orange", error = "#ff0000" }
vim.g.gruvbox_dark_float = true
vim.g.gruvbox_flat_style = "hard"
vim.cmd('colorscheme gruvbox-flat')

-- gruvbox-material
--vim.g.background = 'dark'
--vim.g.gruvbox_material_background = 'hard'
--vim.g.gruvbox_material_enable_italic = 1
--vim.g.gruvbox_material_disable_italic_comment = 0
--vim.g.gruvbox_material_enable_bold = 1
--vim.g.gruvbox_material_ui_contrast = 'low'
--vim.g.gruvbox_material_visual = 'red background'
--vim.g.gruvbox_material_menu_selection_background = 'grey'
--vim.g.gruvbox_material_sign_column_background = 'none'
--vim.g.gruvbox_material_better_performance = 1
--vim.cmd("colorscheme gruvbox-material")


